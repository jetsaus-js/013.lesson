"use strict";

if (4 == 9) {
    console.log('Ok!');
} else {
    console.log('No!');
}

const num1 = 50;
if (num1 < 49) {
    console.log('No!');
} else if (num1 > 100) {
    console.log('Many!');
} else {
    console.log('Ок!');
}

const num2 = 50;
(num2 === 50) ? console.log('Ok!') : console.log('No!');

const num3 = 49;
switch (num3) {
    case 49:
        console.log('No!');
        break;
        case 49:
            console.log('No!');
            break;
    case 50:
        console.log('Ok!');
        break;
    case 100:
        console.log('Many!');
        break;
    default:
        console.log('Muzzily!');
        break;
}